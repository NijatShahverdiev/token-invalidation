package dev.tokeninvalidation.repository;

import dev.tokeninvalidation.domain.UserDevice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<UserDevice, Long> {

    List<UserDevice> findByActive(boolean active);

    boolean existsDeviceById(Long id);
}
