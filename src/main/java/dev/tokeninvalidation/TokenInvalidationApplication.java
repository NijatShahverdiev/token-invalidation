package dev.tokeninvalidation;

import dev.tokeninvalidation.config.cache.TokenBlacklistingService;
import dev.tokeninvalidation.security.domain.AuthUser;
import dev.tokeninvalidation.security.domain.Authority;
import dev.tokeninvalidation.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
//@EnableCaching
public class TokenInvalidationApplication implements CommandLineRunner {
//    private final PasswordEncoder encoder;
    private final UserRepository repository;


    public static void main(String[] args) {
        SpringApplication.run(TokenInvalidationApplication.class, args);
    }

    @Override
    public void run(String... args) {
/*        AuthUser authUser = new AuthUser();
        authUser.setUsername("dct_team");
        authUser.setPassword(encoder.encode("test"));

        Authority adminRole = new Authority();
        adminRole.setAuthority("ROLE_ADMIN");
        Authority userRole = new Authority();
        userRole.setAuthority("ROLE_USER");

        authUser.setAuthorities(List.of(adminRole, userRole));
        repository.save(authUser);*/


//		tokenBlacklistingService.clearCache();

    }
}
