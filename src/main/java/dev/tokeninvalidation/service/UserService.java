package dev.tokeninvalidation.service;

import dev.tokeninvalidation.dto.RefreshTokenDto;
import dev.tokeninvalidation.dto.SignInRequestDto;
import dev.tokeninvalidation.dto.SignInResponseDto;

public interface UserService {

    SignInResponseDto signIn(SignInRequestDto requestDto);
    SignInResponseDto authWithRefreshToken(RefreshTokenDto refreshTokenDto);
}
