package dev.tokeninvalidation.service;

import dev.tokeninvalidation.domain.UserDevice;
import dev.tokeninvalidation.dto.DeviceDto;

import java.util.List;
import java.util.Optional;

public interface DeviceService {

    List<UserDevice> devices(Optional<Boolean> active);

    UserDevice create(DeviceDto deviceDto);

    UserDevice findById(Long deviceId);

    void delete(Long id);

    UserDevice update(DeviceDto deviceDto, Long id);
}
