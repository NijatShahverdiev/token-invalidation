package dev.tokeninvalidation.service.impl;

import dev.tokeninvalidation.common.exception.ApplicationException;
import dev.tokeninvalidation.domain.UserDevice;
import dev.tokeninvalidation.dto.DeviceDto;
import dev.tokeninvalidation.repository.DeviceRepository;
import dev.tokeninvalidation.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static dev.tokeninvalidation.common.exception.ErrorCode.DEVICE_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService {
    private final DeviceRepository deviceRepository;

    @Override
    public List<UserDevice> devices(Optional<Boolean> active) {
        boolean deviceStatus = active.orElse(true);
        return deviceRepository.findByActive(deviceStatus);
    }

    @Override
    public UserDevice create(DeviceDto deviceDto) {
        return deviceRepository.save(new UserDevice(deviceDto.getName(), deviceDto.getVersion(), deviceDto.isActive()));
    }

    @Override
    public UserDevice findById(Long deviceId) {
        return deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ApplicationException(DEVICE_NOT_FOUND, Map.of("id", deviceId)));
    }

    @Override
    public void delete(Long id) {
        UserDevice userDevice = findById(id);
        userDevice.setActive(false);
        deviceRepository.save(userDevice);
    }

    @Override
    public UserDevice update(DeviceDto deviceDto, Long id) {

        UserDevice userDevice = findById(id);
        userDevice.setName(deviceDto.getName());
        userDevice.setVersion(deviceDto.getVersion());
        return deviceRepository.save(userDevice);
    }
}
