package dev.tokeninvalidation.service.impl;

import dev.tokeninvalidation.security.domain.RefreshToken;
import dev.tokeninvalidation.security.repository.RefreshTokenRepository;
import dev.tokeninvalidation.security.service.UserDetailServiceImpl;
import dev.tokeninvalidation.dto.RefreshTokenDto;
import dev.tokeninvalidation.dto.SignInRequestDto;
import dev.tokeninvalidation.dto.SignInResponseDto;
import dev.tokeninvalidation.security.service.JwtService;
import dev.tokeninvalidation.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl /*implements UserService */{

//    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JwtService jwtService;
    private final RefreshTokenRepository refreshTokenRepository;
    private final UserDetailServiceImpl userDetailService;

   /* public SignInResponseDto signIn(SignInRequestDto requestDto) {
        log.info("Authenticating user {}", requestDto.getUsername());
        Authentication authenticationToken = new UsernamePasswordAuthenticationToken(requestDto.getUsername(),
                requestDto.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new SignInResponseDto(jwtService.issueToken(authentication), issueRefreshToken(authentication, null));
    }*/

    /*@Override
    public SignInResponseDto authWithRefreshToken(RefreshTokenDto refreshTokenDto) {
        RefreshToken refreshToken = refreshTokenRepository.findByToken(refreshTokenDto.getToken())
                .orElseThrow(() -> new RuntimeException("Unauthorized"));
        if (refreshToken.isValid() && refreshToken.getEat().after(new Date())) {
            UserDetails userDetails = null; //userDetailService.loadUserByUsername(refreshToken.getUsername());
            if (userDetails.isAccountNonExpired()
                    && userDetails.isAccountNonLocked()
                    && userDetails.isCredentialsNonExpired()
                    && userDetails.isEnabled()) {
                UsernamePasswordAuthenticationToken userPassAuthToken = new UsernamePasswordAuthenticationToken(
                        userDetails.getUsername(), null, userDetails.getAuthorities());
                refreshToken.setValid(false);
                refreshTokenRepository.save(refreshToken);
                return new SignInResponseDto(jwtService.issueToken(userPassAuthToken),
                        issueRefreshToken(userPassAuthToken, refreshToken.getId()));
            }
        }
        throw new RuntimeException("Unauthorized");
    }*/

 /*   private RefreshTokenDto issueRefreshToken(Authentication authentication, Long previousRefreshTokenId) {
        Calendar date = Calendar.getInstance();
        long timeInSec = date.getTimeInMillis();
        Date afterAdd10Mins = new Date(timeInSec + (180L * 24 * 3_600_000));
        RefreshToken refreshToken = RefreshToken
                .builder()
                .username(authentication.getName())
                .token(UUID.randomUUID().toString())
                .eat(afterAdd10Mins)
                .valid(true)
                .previousRefreshTokenId(previousRefreshTokenId)
                .build();
        refreshTokenRepository.save(refreshToken);
        return new RefreshTokenDto(refreshToken.getToken(), refreshToken.getEat());
    }*/

}
