package dev.tokeninvalidation.config.interceptor;

import dev.tokeninvalidation.config.cache.TokenBlacklistingService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;

@Service
@RequiredArgsConstructor
@Slf4j
public class TokenInterceptor implements HandlerInterceptor {

    private final TokenBlacklistingService blacklistingService;

    private final TokenWrapper tokenWrapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        final String authorizationHeaderValue = request.getHeader("Authorization");
        if (authorizationHeaderValue != null && authorizationHeaderValue.startsWith("Bearer")) {

            String token = authorizationHeaderValue.substring(7);
            String blackListedToken = blacklistingService.getJwtBlackList(token);

            if (blackListedToken != null) {
                log.error("Token wos revoked");
                response.sendError(401);
                return false;
            }
            log.error("Token: {}", token);
            tokenWrapper.setToken(token);
            return true;
        }
        return false;
    }
}
