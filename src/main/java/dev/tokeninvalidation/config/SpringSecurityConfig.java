package dev.tokeninvalidation.config;

import lombok.RequiredArgsConstructor;
/*import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;*/


//@EnableWebSecurity
//@EnableMethodSecurity(securedEnabled = true, jsr250Enabled = true)
@RequiredArgsConstructor
//@Configuration
public class SpringSecurityConfig {

    private final JwtAuthFilterConfigAdapter authFilterConfigAdapter;

//    @Bean
    /*public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
        http.apply(authFilterConfigAdapter);
        return http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/api/test").permitAll()
                        .requestMatchers("/auth/sign-in").permitAll()
                        .requestMatchers("/auth/refresh-token").permitAll()
                        .requestMatchers("/auth/change-password").hasAnyRole("ADMIN")
                        .requestMatchers("/demo-user").hasAnyRole("USER", "ADMIN")
                        .requestMatchers("/demo-admin").hasAnyRole("ADMIN")

                        .requestMatchers("/devices/**").hasAnyRole("ADMIN"))
                .httpBasic(Customizer.withDefaults())
                .build();
    }*/


//    @Bean
//    public PasswordEncoder encoder() {
//        return new BCryptPasswordEncoder();
//    }
}
