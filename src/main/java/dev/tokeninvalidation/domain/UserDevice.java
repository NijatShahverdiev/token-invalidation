package dev.tokeninvalidation.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDevice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String version;

    @Column(columnDefinition = "boolean default true")
    private boolean active;

    public UserDevice(String name, String version){
        this.name = name;
        this.version = version;
    }

    public UserDevice(String name, String version, boolean active) {
        this.name = name;
        this.version = version;
        this.active = active;
    }
}
