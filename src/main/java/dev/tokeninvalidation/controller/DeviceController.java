package dev.tokeninvalidation.controller;


import dev.tokeninvalidation.domain.UserDevice;
import dev.tokeninvalidation.dto.DeviceDto;
import dev.tokeninvalidation.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/devices")
@Slf4j
public class DeviceController {

    private final DeviceService deviceService;

    @GetMapping
    public List<UserDevice> devices(@RequestParam(required = false) Optional<Boolean> active) {
        return deviceService.devices(active);
    }

    @GetMapping("{id}")
    public UserDevice findDeviceById(@PathVariable(name = "id") Long id){
        return deviceService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDevice create(@RequestBody DeviceDto deviceDto){
        return deviceService.create(deviceDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDevice(@RequestBody DeviceDto deviceDto, @PathVariable(name = "id") Long id){
        deviceService.update(deviceDto, id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDevice(@PathVariable(name = "id") Long id){
        deviceService.delete(id);
    }
}
