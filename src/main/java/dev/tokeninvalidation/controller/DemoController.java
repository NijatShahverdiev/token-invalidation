package dev.tokeninvalidation.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
@RequiredArgsConstructor
@Slf4j
public class DemoController {


    @GetMapping("/api/test")
    public String demo(){
        log.info("hello");
        return " test";
    }

    @GetMapping("/demo2")
    public String demo2(){
        return "demo2";
    }

    @GetMapping("/demo-user")
    public String demoUser(){
        return "User page";
    }

    @GetMapping("/demo-admin")
    public String demoAdmin(){
        return "Admin page";
    }
}
