package dev.tokeninvalidation.controller;


import dev.tokeninvalidation.config.cache.TokenBlacklistingService;
import dev.tokeninvalidation.config.interceptor.TokenWrapper;
import dev.tokeninvalidation.security.service.UserDetailServiceImpl;
import dev.tokeninvalidation.dto.RefreshTokenDto;
import dev.tokeninvalidation.dto.SignInRequestDto;
import dev.tokeninvalidation.dto.SignInResponseDto;
import dev.tokeninvalidation.dto.UserCredentialChange;
import dev.tokeninvalidation.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping
@Slf4j
public class AuthController {

//    private final UserService userService;
    private final UserDetailServiceImpl userDetailService;

    private final TokenBlacklistingService blackListingService;
    private final TokenWrapper tokenWrapper;

    @PutMapping("/auth/change-password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void changePassword(@RequestBody @Valid UserCredentialChange userCredential){
        userDetailService.changePassword(userCredential);
    }

  /*  @PostMapping("/auth/sign-in")
    public SignInResponseDto signIn(@RequestBody @Valid SignInRequestDto requestDto){
        return userService.signIn(requestDto);
    }

    @PostMapping("/auth/refresh-token")
    public SignInResponseDto signIn(@RequestBody @Valid RefreshTokenDto refreshTokenDto){
        return userService.authWithRefreshToken(refreshTokenDto);
    }*/
}
