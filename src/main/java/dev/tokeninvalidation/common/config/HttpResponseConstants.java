package dev.tokeninvalidation.common.config;

public final class HttpResponseConstants {

    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String PATH = "path";
    public static final String ERROR = "error";
    public static final String ERRORS = "errors";
    private HttpResponseConstants() {
    }
}


