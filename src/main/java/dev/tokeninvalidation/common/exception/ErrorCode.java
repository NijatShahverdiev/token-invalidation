package dev.tokeninvalidation.common.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode implements ErrorResponse {

    UNAUTHORIZED("UNAUTHORIZED", HttpStatus.UNAUTHORIZED, "Invalid credentials"),
    DEVICE_NOT_FOUND("DEVICE_NOT_FOUND", HttpStatus.NOT_FOUND,
            "DEVICE not found with the given id: {id}");


    final String key;
    final HttpStatus httpStatus;
    final String message;

    ErrorCode(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
