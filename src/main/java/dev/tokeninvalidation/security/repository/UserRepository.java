package dev.tokeninvalidation.security.repository;

import dev.tokeninvalidation.security.domain.AuthUser;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<AuthUser, Long> {
//    @EntityGraph(attributePaths = {"authorities"})
//    UserDetails findByUsername(String username);

}
