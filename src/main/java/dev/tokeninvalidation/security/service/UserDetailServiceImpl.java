package dev.tokeninvalidation.security.service;

import dev.tokeninvalidation.config.cache.TokenBlacklistingService;
import dev.tokeninvalidation.config.interceptor.TokenWrapper;
import dev.tokeninvalidation.security.repository.UserRepository;
import dev.tokeninvalidation.dto.UserCredentialChange;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailServiceImpl /*implements UserDetailsService */{

    private final UserRepository userRepository;
//    private final PasswordEncoder passwordEncoder;
    private final TokenBlacklistingService blackListingService;
    private final TokenWrapper tokenWrapper;

/*    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }*/

    public void changePassword(UserCredentialChange userCredential) {
        /*AuthUser authUser = (AuthUser) loadUserByUsername(userCredential.username());

        if (passwordEncoder.matches(userCredential.oldPassword(), authUser.getPassword())) {
            authUser.setPassword(passwordEncoder.encode(userCredential.newPassword()));
            blackListingService.blackListJwt(tokenWrapper.getToken());
            userRepository.save(authUser);
        }else
            throw new IllegalStateException("Bad Credential");*/
    }
}
