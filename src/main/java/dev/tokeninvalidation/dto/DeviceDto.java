package dev.tokeninvalidation.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceDto {

    @NotBlank
    private String name;

    private String version;

    private boolean active = true;
}
