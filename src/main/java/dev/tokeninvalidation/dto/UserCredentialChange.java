package dev.tokeninvalidation.dto;

public record UserCredentialChange(String username, String oldPassword, String newPassword) {
}
