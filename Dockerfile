FROM openjdk:17-jdk-slim
WORKDIR /app
COPY build/libs/demo-app-0.1.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/app.jar"]